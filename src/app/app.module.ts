import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { Page2Component } from './page2/page2.component';
import { Page3Component } from './page3/page3.component';
import { Page1Component } from './page1/page1.component';
import { SamplePageComponent } from './sample-page/sample-page.component';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    Page2Component,
    Page3Component,
    Page1Component,
    SamplePageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [ AppRoutingModule ]
})
export class AppModule { 

}
